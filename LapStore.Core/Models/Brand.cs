﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LapStore.Core.Models
{
    public class Brand
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Category name is required.")]
        [StringLength(255)]
        [Display(Name="Tên")]
        public string Name { get; set; }

        [StringLength(255)]
        public string? UrlSlug { get; set; }

        [StringLength(1024)]
        [Column(TypeName = "ntext")]
        [Display(Name = "Mô tả")]
        public string? Description { get; set; }
        [ValidateNever]
        public ICollection<Product> Products { get; set; }
    }
}