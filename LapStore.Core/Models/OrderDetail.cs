﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapStore.Core.Models
{
    public class OrderDetail
    {
        [Key]
        [Column(Order = 1)]
        public int OrderID { set; get; }

        [Key]
        [Column(Order = 2)]
        public int ProductId { set; get; }
        [Display(Name = "Số lượng")]
        public int Quantity { set; get; }
        [Display(Name = "Giá")]
        public decimal Price { set; get; }

        public virtual Order Order { set; get; }

        public virtual Product Product { set; get; }
    }
}
