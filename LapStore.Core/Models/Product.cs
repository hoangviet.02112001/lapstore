﻿
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LapStore.Core.Models
{
    public class Product
    {
     
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name product is required.")]
        [StringLength(255)]
        //[TitleValid]
        public string Name { get; set; }

        [StringLength(1024)]
        [Column(TypeName = "ntext")]
        public string? ImageName { get; set; }

        [Column(TypeName = "xml")]
        public string? MoreImages { set; get; }

        [StringLength(255)]
        public string? UrlSlug { get; set; }
        public float Monitor { get; set; }
        public string Cpu { get; set; }
        public int Ram { get; set; }
        public int HardDriver { get; set; }
        public string Card { get; set; }
        public string OperatingSystem { get; set; } 
        public float Weight { get; set; }
        public string Size { get; set; }
        public decimal Price { set; get; }
        public int DiscountId { get; set; }
        public int Quantity { set; get; }
        public int? RateCount { get; set; } = 0;
        public int? TotalRate { get; set; } = 0;
        public decimal Rate
        {
            get
            {
                if(TotalRate == null || RateCount == null)
                    return 0;
                if (TotalRate.Value == 0 || RateCount.Value == 0)
                    return 0;
                return Convert.ToDecimal(1.0 * TotalRate.Value / RateCount.Value);
            }
        }
        public int BrandId { get; set; }
        [ValidateNever]
        public Brand Brand { get; set; }
        [ValidateNever]
        public ICollection<Comment> Comments { get; set; }
        [ValidateNever]
        public ICollection<ProductTagMap> ProductTagMap { get; set; }
    }
    //public class TitleValidAttribute : ValidationAttribute
    //{
    //    protected override ValidationResult IsValid(object value,
    //    ValidationContext validationContext)
    //    {
    //        var context = (LapStoreContext)validationContext.GetService(typeof(LapStoreContext));
    //        if (!context.Products.Any(a => a.Title == value.ToString()))
    //        {
    //            return ValidationResult.Success;
    //        }
    //        return new ValidationResult("Title exists");
    //    }
    //}
}