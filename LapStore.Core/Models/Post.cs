﻿using LapStore.Core.Data;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LapStore.Core.Models
{
    public class Post
    {
     
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Post title is required.")]
        [StringLength(255)]
        //[TitleValid]
        [Display(Name = "Tiêu đề")]
        public string Title { get; set; }

        [StringLength(1024)]
        [Column(TypeName = "ntext")]
        [Display(Name = "Mô tả")]
        public string? ShortDescription { get; set; }
        [Display(Name = "Ảnh")]
        public string? ImageLink { get; set; }

        [Column(TypeName = "ntext")]
        [Display(Name = "Nội dung")]
        public string? PostContent { get; set; }

        [StringLength(255)]
        public string? UrlSlug { get; set; }
        [Display(Name = "Trạng thái đăng")]
        public bool? Published { get; set; }
        [Display(Name = "Thời gian đăng")]
        public DateTime? PostedOn { get; set; }
        [Display(Name = "Thời gian chỉnh sửa")]
        public DateTime? Modified { get; set; }
        [Display(Name = "Lượt xem")]
        public int? ViewCount { get; set; } = 0;
        [Display(Name = "Lượt đánh giá")]
        public int? RateCount { get; set; } = 0;
        [Display(Name = "Tổng đánh giá")]
        public int? TotalRate { get; set; } = 0;
        [Display(Name = "Đánh giá")]
        public decimal Rate
        {
            get
            {
                if(TotalRate == null || RateCount == null)
                    return 0;
                if (TotalRate.Value == 0 || RateCount.Value == 0)
                    return 0;
                return Convert.ToDecimal(1.0 * TotalRate.Value / RateCount.Value);
            }
        }

        public int CategoryId { get; set; }
        [ValidateNever]
        public Category Category { get; set; }
        [ValidateNever]
        public ICollection<Comment> Comments { get; set; }
        [ValidateNever]
        public ICollection<PostTagMap> PostTagMap { get; set; }
    }

}