﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapStore.Core.Models
{
    public class Comment
    {
        public int Id { get; set; }

        [Display(Name = "Tên")]
        public string Name { get; set; }
        public string? Email { get; set; }
        public int PostId { get; set; }

        [Display(Name = "Tiêu đề")]
        public string? CommentHeader { get; set; }

        [Display(Name = "Nội dung")]
        public string? CommentText { get; set; }

        [Display(Name = "Thời gian")]
        public DateTime? CommentTime { get; set; }
        [ValidateNever]
        public Post Post { get; set; }
    }
}
