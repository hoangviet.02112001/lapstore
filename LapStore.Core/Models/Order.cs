﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapStore.Core.Models
{
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }
        [MaxLength(256)]
        [Display(Name = "Phương thức thanh toán")]
        public int PaymentMethod { set; get; }
        [Display(Name = "Trạng thái")]
        public int PaymentStatus { set; get; }
        [Display(Name = "Ngày tạo")]
        public DateTime? CreatedDate { set; get; }
        [MaxLength(450)]
        [Column(TypeName = "nvarchar")]
        public string CustomerId { set; get; }

        public virtual ApplicationUser User { set; get; }

        public virtual ICollection<OrderDetail> OrderDetails { set; get; }
    }
}
