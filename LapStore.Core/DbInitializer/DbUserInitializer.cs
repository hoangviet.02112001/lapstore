﻿
using LapStore.Common;
using LapStore.Core.Data;
using LapStore.Core.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Data;


namespace LapStore.Core.DbInitializer
{
    public class DbUserInitializer : IEntityTypeConfiguration<ApplicationUser>
    {
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            var hasher = new PasswordHasher<IdentityUser>();
            builder.HasData(
                new ApplicationUser
                {
                    Id = "08bc2fc1-5387-4033-b7d3-8208d29746ff",
                    Name = "User",
                    Email = "user@localhost.com",
                    NormalizedEmail = "USER@LOCALHOST.COM",
                    NormalizedUserName = "USER@LOCALHOST.COM",
                    UserName = "user@localhost.com",
                    PasswordHash = hasher.HashPassword(null, "P@ssword1"),
                    EmailConfirmed = true,
                    StreetAddress = "Hà Nội"

                },
                new ApplicationUser
                {
                    Id = "9fa0160f-c2c6-4124-8221-4f465e979807",
                    Name="Admin",
                    Email = "Admin@localhost.com",
                    NormalizedEmail = "ADMIN@LOCALHOST.COM",
                    NormalizedUserName = "ADMIN@LOCALHOST.COM",
                    UserName = "admin@localhost.com",
                    PasswordHash = hasher.HashPassword(null, "P@ssword1"),
                    EmailConfirmed = true,
                    StreetAddress = "Hà Nam"
                }
                );
        }
    }
}
