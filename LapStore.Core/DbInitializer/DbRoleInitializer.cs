﻿
using LapStore.Common;
using LapStore.Core.Data;
using LapStore.Core.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Data;


namespace LapStore.Core.DbInitializer
{
    public class DbRoleInitializer : IEntityTypeConfiguration<IdentityRole>
    {
        public void Configure(EntityTypeBuilder<IdentityRole> builder)
        {
            builder.HasData(
            new IdentityRole
            {
                Id = "58cd381e-8683-4b42-b7b6-70b6274bfa84",
                Name = SD.Role_Admin,
                NormalizedName = SD.Role_Admin.ToUpper()
            },
            new IdentityRole
            {
                Id = "97ddd633-2a4d-42b3-8f82-a5fe1d94173a",
                Name = SD.Role_User,
                NormalizedName = SD.Role_User.ToUpper()
            }
            );
        }
    }
}
