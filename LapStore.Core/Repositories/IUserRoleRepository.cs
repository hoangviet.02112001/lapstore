﻿using LapStore.Core.Infrastructures;
using LapStore.Core.Models;
using Microsoft.AspNetCore.Identity;

namespace LapStore.Core.Repositories
{
    public interface IUserRoleRepository : IGenericRepository<IdentityUserRole<string>>
    {
        public IdentityUserRole<string> GetRoleByUserID(string id);
    }
}
