﻿using LapStore.Common;
using LapStore.Core.Data;
using LapStore.Core.Infrastructures;
using LapStore.Core.Models;
using Microsoft.AspNetCore.Identity;

namespace LapStore.Core.Repositories

{
    public class UserRoleRepository : GenericRepository<IdentityUserRole<string>>, IUserRoleRepository
    {
        public UserRoleRepository(LapStoreContext context) : base(context)
        {
        }

        public IdentityUserRole<string> GetRoleByUserID(string id)
        {
            return context.Set<IdentityUserRole<string>>().Where(x=>x.UserId.Equals(id)).FirstOrDefault(); 
        }
    }
}
