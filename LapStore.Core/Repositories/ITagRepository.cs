﻿using LapStore.Core.Infrastructures;
using LapStore.Core.Models;

namespace LapStore.Core.Repositories
{
    public interface ITagRepository : IGenericRepository<Tag>
    {
        //Tag Find(int TagId);

        //void AddTag(Tag Tag);

        //void UpdateTag(Tag Tag);

        //void DeleteTag(Tag Tag);

        //void DeleteTag(int TagId);

        //IList<Tag> GetAllTags();

        Tag GetTagByUrlSlug(string urlSlug);
        IList<Tag> GetTagsByPost(int postId);
        public IEnumerable<int> AddTagByString(string tags);
    }
}