﻿using LapStore.Core.Data;
using LapStore.Core.Infrastructures;
using LapStore.Core.Models;

namespace LapStore.Core.Repositories
{
    public class BrandRepository : GenericRepository<Brand>, IBrandRepository
    {
        public BrandRepository(LapStoreContext context) : base(context)
        {
        }

        
    }
}