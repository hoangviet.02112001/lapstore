﻿using LapStore.Core.Infrastructures;
using LapStore.Core.Models;

namespace LapStore.Core.Repositories
{
    public interface IUserRepository : IGenericRepository<ApplicationUser>
    {
        public List<ApplicationUser> GetByOrderByDescending();
        public ApplicationUser GetByEmail(string email);
    }
}
