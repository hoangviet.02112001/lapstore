﻿using LapStore.Core.Infrastructures;
using LapStore.Core.Models;

namespace LapStore.Core.Repositories
{
    public interface IBrandRepository : IGenericRepository<Brand>
    {
        //Brand Find(int BrandId);
        //void AddBrand(Brand Brand);
        //void UpdateBrand(Brand Brand);
        //void DeleteBrand(Brand Brand);
        //void DeleteBrand(int BrandId);
        //IList<Brand> GetAllCategories();
    }
}