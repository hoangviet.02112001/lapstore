﻿using LapStore.Core.Infrastructures;
using LapStore.Core.Models;

namespace LapStore.Core.Repositories
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
        //Category Find(int categoryId);
        //void AddCategory(Category category);
        //void UpdateCategory(Category category);
        //void DeleteCategory(Category category);
        //void DeleteCategory(int categoryId);
        //IList<Category> GetAllCategories();
    }
}