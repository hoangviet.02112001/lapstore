﻿using LapStore.Core.Infrastructures;
using LapStore.Core.Models;

namespace LapStore.Core.Repositories
{
    public interface IDiscountRepository : IGenericRepository<Discount>
    {

    }
}