﻿using LapStore.Core.Data;
using LapStore.Core.Infrastructures;
using LapStore.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace LapStore.Core.Repositories
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(LapStoreContext context) : base(context)
        {
        }

    }
}