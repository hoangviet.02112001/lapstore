﻿using LapStore.Common;
using LapStore.Core.Data;
using LapStore.Core.Infrastructures;
using LapStore.Core.Models;

namespace LapStore.Core.Repositories
{
    public class UserRepository : GenericRepository<ApplicationUser>, IUserRepository
    {
        public UserRepository(LapStoreContext context) : base(context)
        {
        }

        public ApplicationUser GetByEmail(string email)
        {
            return context.Set<ApplicationUser>().Where(x => x.Email.Equals(email)).FirstOrDefault();
        }

        public List<ApplicationUser> GetByOrderByDescending()
        {
           return context.Set<ApplicationUser>().OrderByDescending(anhxa => anhxa.Id).ToList();
        }
    }
}
