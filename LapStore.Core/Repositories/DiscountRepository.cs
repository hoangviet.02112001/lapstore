﻿using LapStore.Common;
using LapStore.Core.Data;
using LapStore.Core.Infrastructures;
using LapStore.Core.Models;

namespace LapStore.Core.Repositories
{
    public class DiscountRepository : GenericRepository<Discount>, IDiscountRepository
    {
        public DiscountRepository(LapStoreContext context) : base(context)
        {
        }


    }
}