﻿using LapStore.Core.Data;
using LapStore.Core.Infrastructures;
using LapStore.Core.Models;

namespace LapStore.Core.Repositories
{
    public class CommentRepository : GenericRepository<Comment>, ICommentRepository
    {
        public CommentRepository(LapStoreContext context) : base(context)
        {
        }

        public void AddComment(int postId, string commentName, string commentEmail, string commentTitle, string commentBody)
        {
            this.dbSet.Add(new Comment { PostId = postId, Name = commentName, Email = commentEmail, CommentHeader = commentTitle, CommentText = commentBody });
        }

        public IList<Comment> GetCommentsForPost(int postId)
        {
            return this.dbSet.Where(x => x.PostId == postId).ToList();
        }

        public IList<Comment> GetCommentsForPost(Post post)
        {
            return this.dbSet.Where(x => x.PostId == post.Id).ToList();
        }
    }
}