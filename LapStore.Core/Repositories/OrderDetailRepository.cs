﻿using LapStore.Core.Data;
using LapStore.Core.Infrastructures;
using LapStore.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapStore.Core.Repositories
{
    public class OrderDetailRepository : GenericRepository<OrderDetail>, IOrderDetailRepository
    {
        public OrderDetailRepository(LapStoreContext context) : base(context)
        {
        }

    }
}
