﻿using LapStore.Core.Infrastructures;
using LapStore.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapStore.Core.Repositories
{
    public interface IOrderDetailRepository : IGenericRepository<OrderDetail>
    {

        //OrderDetail FindOrderDetail(int OrderDetailId);

        //void AddOrderDetail(OrderDetail OrderDetail);

        //void UpdateOrderDetail(OrderDetail OrderDetail);

        //void DeleteOrderDetail(OrderDetail OrderDetail);

        //void DeleteOrderDetail(int OrderDetailId);

        //IList<OrderDetail> GetAllOrderDetails();

    }
}
