﻿using LapStore.Core.Data;
using LapStore.Core.Infrastructures;
using LapStore.Core.Models;

namespace LapStore.Core.Repositories
{
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(LapStoreContext context) : base(context)
        {
        }

        
    }
}