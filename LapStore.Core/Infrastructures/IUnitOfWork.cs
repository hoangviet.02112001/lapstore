﻿using LapStore.Core.Data;
using LapStore.Core.Models;
using LapStore.Core.Repositories;

namespace LapStore.Core.Infrastructures
{
    public interface IUnitOfWork : IDisposable
    {
        ICategoryRepository CategoryRepository { get; }
        IPostRepository PostRepository { get; }
        ITagRepository TagRepository { get; }
        ICommentRepository CommentRepository { get; }
        IBrandRepository BrandRepository {get; }
        IProductRepository ProductRepository {get; }
        IOrderRepository OrderRepository {get; }
        IOrderDetailRepository OrderDetailRepository{get; }
        IUserRepository UserRepository { get; }
        IUserRoleRepository UserRoleRepository { get; }
        IDiscountRepository DiscountRepository { get; }
        LapStoreContext LapStoreContext { get; }

        int SaveChange();

        Task<int> SaveChangeAsync();
    }
}