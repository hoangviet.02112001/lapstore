﻿
using LapStore.Core.Data;
using LapStore.Core.Repositories;

namespace LapStore.Core.Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly LapStoreContext context;
        private ICategoryRepository categoryRepository;
        private IPostRepository postRepository;
        private ITagRepository tagRepository;
        private ICommentRepository commentRepository;
        private IBrandRepository brandRepository;
        private IProductRepository productRepository;
        private IOrderRepository orderRepository;
        private IOrderDetailRepository orderDetailRepository;
        private IUserRepository userRepository;
        private IUserRoleRepository userRoleRepository;
        private IDiscountRepository discountRepository;

        public UnitOfWork(LapStoreContext context)
        {
            this.context = context;
        }

        public ICategoryRepository CategoryRepository
        {
            get
            {
                if (this.categoryRepository == null)
                {
                    this.categoryRepository = new CategoryRepository(this.context);
                }
                return this.categoryRepository;
            }
        }

        public IPostRepository PostRepository
        {
            get
            {
                if (this.postRepository == null)
                {
                    this.postRepository = new PostRepository(this.context);
                }
                return this.postRepository;
            }
        }

        public ITagRepository TagRepository
        {
            get
            {
                if (this.tagRepository == null)
                {
                    this.tagRepository = new TagRepository(this.context);
                }
                return this.tagRepository;
            }
        }

        public LapStoreContext LapStoreContext => this.context;

        public ICommentRepository CommentRepository
        {
            get
            {
                if (this.commentRepository == null)
                {
                    this.commentRepository = new CommentRepository(this.context);
                }
                return this.commentRepository;
            }
        }

        public IBrandRepository BrandRepository 
        {
            get
            {
                if (this.brandRepository == null)
                {
                    this.brandRepository = new BrandRepository(this.context);
                }
                return this.brandRepository;
            }
        }
        public IProductRepository ProductRepository
        {
            get
            {
                if (this.productRepository == null)
                {
                    this.productRepository = new ProductRepository(this.context);
                }
                return this.productRepository;
            }
        }
        public IOrderRepository OrderRepository
        {
            get
            {
                if (this.orderRepository == null)
                {
                    this.orderRepository = new OrderRepository(this.context);
                }
                return this.orderRepository;
            }
        }
        public IOrderDetailRepository OrderDetailRepository
        {
            get
            {
                if (this.orderDetailRepository == null)
                {
                    this.orderDetailRepository = new OrderDetailRepository(this.context);
                }
                return this.orderDetailRepository;
            }
        }
        public IUserRepository UserRepository
        {
            get
            {
                if (this.userRepository == null)
                {
                    this.userRepository = new UserRepository(this.context);
                }
                return this.userRepository;
            }
        }
        public IUserRoleRepository UserRoleRepository
        {
            get
            {
                if (this.userRoleRepository == null)
                {
                    this.userRoleRepository = new UserRoleRepository(this.context);
                }
                return this.userRoleRepository;
            }
        }
        public IDiscountRepository DiscountRepository
        {
            get
            {
                if (this.discountRepository == null)
                {
                    this.discountRepository = new DiscountRepository(this.context);
                }
                return this.discountRepository;
            }
        }

        public void Dispose()
        {
            this.context.Dispose();
        }

        public int SaveChange()
        {
            return this.context.SaveChanges();
        }

        public async Task<int> SaveChangeAsync()
        {
            return await this.context.SaveChangesAsync();
        }
    }
}