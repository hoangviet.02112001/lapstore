﻿using LapStore.Core.DbInitializer;
using LapStore.Core.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace LapStore.Core.Data
{
    public class LapStoreContext : IdentityDbContext<ApplicationUser>
    {
        public LapStoreContext(DbContextOptions<LapStoreContext> options) : base(options)
        {
        }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<Tag> Tags { get; set; }
        public virtual DbSet<PostTagMap> PostTagMap { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductTagMap> ProductTagMap { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderDetail> OrderDetails { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Post>(entity =>
            {
                entity.Property(e => e.Modified)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PostedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
                entity.Property(e => e.PostContent).HasMaxLength(int.MaxValue);

                entity.Property(e => e.Published).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Posts)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_PostsCategoryId");
               

            });
            modelBuilder.Entity<PostTagMap>(entity =>
            {

                entity.HasKey(pt => new { pt.PostId, pt.TagId });
                entity.HasOne(x => x.Post).WithMany(y => y.PostTagMap).HasForeignKey(x => x.PostId).OnDelete(DeleteBehavior.Cascade);
                entity.HasOne(x => x.Tag).WithMany(y => y.PostTagMap).HasForeignKey(x => x.TagId).OnDelete(DeleteBehavior.Cascade);
            });
            modelBuilder.Entity<Comment>(entity =>
            {
                entity.Property(n => n.Name).HasMaxLength(255);
                entity.Property(c=>c.CommentHeader).HasMaxLength(255);
                entity.Property(c => c.CommentText).HasColumnType("ntext");
                entity.Property(e => e.CommentTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
                entity.HasOne(p => p.Post)
                      .WithMany(c => c.Comments)
                      .HasForeignKey(p => p.PostId)
                      .OnDelete(DeleteBehavior.Cascade);
                  
            });
            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.Price).HasColumnType("decimal(18,2)");

                entity.HasOne(d => d.Brand)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.BrandId)
                    .HasConstraintName("FK_ProductsCategoryId");


            });
            modelBuilder.Entity<ProductTagMap>(entity =>
            {

                entity.HasKey(pt => new { pt.ProductId, pt.TagId });
                entity.HasOne(x => x.Product).WithMany(y => y.ProductTagMap).HasForeignKey(x => x.ProductId).OnDelete(DeleteBehavior.Cascade);
                entity.HasOne(x => x.Tag).WithMany(y => y.ProductTagMap).HasForeignKey(x => x.TagId).OnDelete(DeleteBehavior.Cascade);
            });
            modelBuilder.Entity<Order>(entity =>
            {

                entity.HasKey(pt => new { pt.Id });
                entity.HasOne(d => d.User)
                .WithMany(p => p.Orders)
                .HasForeignKey(d => d.CustomerId);
            });
            modelBuilder.Entity<OrderDetail>(entity =>
            {
                entity.Property(e => e.Price).HasColumnType("decimal(18,2)");

                entity.HasKey(pt => new { pt.OrderID, pt.ProductId });
            });

            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new DbRoleInitializer());
            modelBuilder.ApplyConfiguration(new DbUserInitializer());
            modelBuilder.ApplyConfiguration(new DbUserRoleInitializer());
            modelBuilder.Seed();
            
        }
        public override EntityEntry Add(object entity)
        {
            return base.Add(entity);    
        }
        public override EntityEntry<TEntity> Add<TEntity>(TEntity entity)
        {
            return base.Add(entity);
        }

    }
}