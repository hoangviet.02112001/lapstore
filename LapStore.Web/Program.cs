﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using LapStore.Core.Infrastructures;
using EFCore.DbContextFactory.Extensions;
using System.Text.Json.Serialization;
using LapStore.Core.Data;
using LapStore.Core.DbInitializer;
using Microsoft.AspNetCore.Identity.UI.Services;
using LapStore.Core.Models;
using LapStore.Web.Services.Mail;
using System.Configuration;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddDistributedMemoryCache();

builder.Services.AddSession(cfg => {             
    cfg.Cookie.Name = "LapStore";            
    cfg.IdleTimeout = new TimeSpan(0, 30, 0);    
});
builder.Services.AddDbContext<LapStoreContext>(options => options.UseSqlServer(
    builder.Configuration.GetConnectionString("DefaultConnection")
    ));

builder.Services.AddIdentity<ApplicationUser, IdentityRole>().AddDefaultTokenProviders()
    .AddEntityFrameworkStores<LapStoreContext>();
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();

//Dịch vụ send mail
builder.Services.AddOptions();                                        // Kích hoạt Options
var mailsettings = builder.Configuration.GetSection("MailSettings");  // đọc config
builder.Services.Configure<MailSettings>(mailsettings);               // đăng ký để Inject
builder.Services.AddTransient<IEmailSender, SendMailService>();

builder.Services.AddRazorPages();
builder.Services.AddControllers().AddJsonOptions(x =>
                x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);


builder.Services.ConfigureApplicationCookie(options =>
{
    options.LoginPath = $"/Identity/Account/Login";
    options.LogoutPath = $"/Identity/Account/Logout";
    options.AccessDeniedPath = $"/Identity/Account/AccessDenied";
});
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();
app.UseSession();
app.MapRazorPages();
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(
        name: "Post",
        pattern: "Post/{year}/{month}/{title}",
        new { area = "Customer", controller = "Post", action = "Details" },
        new { year = @"\d{4}", month = @"\d{2}" });
    endpoints.MapControllerRoute(
         name: "default",
          pattern: "{area=Customer}/{controller=Products}/{action=Index}/{id?}");

});
app.Run();