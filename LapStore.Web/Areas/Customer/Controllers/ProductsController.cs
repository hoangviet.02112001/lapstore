﻿using LapStore.Core.Infrastructures;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using X.PagedList;

namespace LapStore.Web.Areas.Customer.Controllers
{
    [Area("Customer")]
    public class ProductsController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public ProductsController( IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public IActionResult Index(string search="",int? page = 1)
        {
            if (page != null && page < 1)
            {
                page = 1;
            }

            var pageSize = 6;
            if (search == null) {
                var listProduct = unitOfWork.ProductRepository.GetAll().ToPagedList(page ?? 1, pageSize);

                return View(listProduct);
            }
            else
            {
                var listProduct = unitOfWork.ProductRepository.GetAll().Where(x => x.Name.Contains(search)).ToPagedList(page ?? 1, pageSize);

                return View(listProduct);
            }

        }
        public IActionResult Detail(int id)
        {
            var Product = unitOfWork.ProductRepository.GetById(id);
            return View(Product);
        }
        public IActionResult Details(int id)
        {
            var Product = unitOfWork.ProductRepository.GetById(id);
            if (Product == null)
                return NotFound();
            unitOfWork.ProductRepository.Update(Product);
            unitOfWork.SaveChange();
            return View(Product);
        }
        public IActionResult GetProductByBrand(string brandUrl, string search = "", int? page = 1)
        {
            if (page != null && page < 1)
            {
                page = 1;
            }

            var pageSize = 6;
            if (search == null)
            {
                var listProduct = unitOfWork.ProductRepository.GetAll(includeProperties: "Brand").Where(x=>x.Brand.UrlSlug.Equals(brandUrl)).ToPagedList(page ?? 1, pageSize);

                return View("Index",listProduct);
            }
            else
            {
                var listProduct = unitOfWork.ProductRepository.GetAll(includeProperties: "Brand").Where(x => x.Brand.UrlSlug.Equals(brandUrl)).Where(x => x.Name.Contains(search)).ToPagedList(page ?? 1, pageSize);

                return View("Index",listProduct);
            }
        }

    }
}
