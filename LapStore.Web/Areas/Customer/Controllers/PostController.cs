﻿using LapStore.Core.Infrastructures;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using X.PagedList;

namespace LapStore.Web.Areas.Customer.Controllers
{
    [Area("Customer")]
    public class PostController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public PostController( IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        //public IActionResult Index()
        //{
        //    var listPost = unitOfWork.PostRepository.GetAll(includeProperties: "Category");
        //    return View(listPost);
        //}
        public IActionResult Index(string search="",int? page = 1)
        {
            if (page != null && page < 1)
            {
                page = 1;
            }

            var pageSize = 6;
            if (search == null) {
                var listPost = unitOfWork.PostRepository.GetAll(includeProperties: "Category").ToPagedList(page ?? 1, pageSize);

                return View(listPost);
            }
            else
            {
                var listPost = unitOfWork.PostRepository.GetAll(includeProperties: "Category").Where(x => x.Title.Contains(search)).ToPagedList(page ?? 1, pageSize);

                return View(listPost);
            }

        }
        public IActionResult LastestPost()
        {
            return PartialView(unitOfWork.PostRepository.GetLatestPost(5));

        }
        public IActionResult Detail(int id)
        {
            var post = unitOfWork.PostRepository.GetById(id);
            return View(post);
        }
        public IActionResult Details(int year, int month, string title)
        {
            var post = unitOfWork.PostRepository.FindPost(year, month, title);
            if (post == null)
                return NotFound();
            post.ViewCount++;
            unitOfWork.PostRepository.Update(post);
            unitOfWork.SaveChange();
            return View(post);
        }
        public IActionResult GetPostByCategory(string categoryName, string search = "", int? page = 1)
        {
            if (page != null && page < 1)
            {
                page = 1;
            }

            var pageSize = 6;
            if (search == null)
            {
                var listPost = unitOfWork.PostRepository.GetAll(includeProperties: "Category").Where(x =>x.Category.UrlSlug.Equals(categoryName)).ToPagedList(page ?? 1, pageSize);

                return View("Index",listPost);
            }
            else
            {
                var listPost = unitOfWork.PostRepository.GetAll(includeProperties: "Category").Where(x => x.Category.UrlSlug.Equals(categoryName)).Where(x => x.Title.Contains(search)).ToPagedList(page ?? 1, pageSize);

                return View("Index",listPost);
            }
        }
        public IActionResult GetPostByTag(string tagName, string search = "", int? page = 1)
        {
            if (page != null && page < 1)
            {
                page = 1;
            }

            var pageSize = 6;
            if (search == null)
            {
                var listPost = unitOfWork.PostRepository.GetPostsByTag(tagName).ToPagedList(page ?? 1, pageSize);

                return View("Index", listPost);
            }
            else
            {
                var listPost = unitOfWork.PostRepository.GetPostsByTag(tagName).Where(x => x.Title.Contains(search)).ToPagedList(page ?? 1, pageSize);

                return View("Index", listPost);
            }
        }

    }
}
