﻿using LapStore.Common;
using LapStore.Core.Infrastructures;
using LapStore.Core.Models;
using LapStore.ModelViews.PostViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LapStore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = SD.Role_Admin)]
    public class ProductsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProductsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {
            return View();
        }

        //GET
        public IActionResult Create()
        {
            ViewBag.CategoryList = _unitOfWork.BrandRepository.GetAll().Select(i => new SelectListItem
            {
                Text = i.Name,
                Value = i.Id.ToString()
            });
            return View();
        }

        //Product
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Product request)
        {

            if (ModelState.IsValid)
            {
                request.UrlSlug = SeoUrlHepler.FrientlyUrl(request.Name);
                this._unitOfWork.ProductRepository.Add(request);
                _unitOfWork.SaveChange();
                TempData["success"] = "Product created successfully";
                return RedirectToAction("Index");
            }
            TempData["error"] = "Product created error";
            return View(request);
        }

        //GET
        public IActionResult Edit(int? id)
        {

            ViewBag.CategoryList = _unitOfWork.BrandRepository.GetAll().Select(i => new SelectListItem
            {
                Text = i.Name,
                Value = i.Id.ToString()
            });
            
            if (id.Value == null || id.Value == 0)
            {
                return NotFound();
            }
            var ProductFromDbFirst = _unitOfWork.ProductRepository.GetById(id.Value);
            if (ProductFromDbFirst == null)
            {
                return NotFound();
            }

            return View(ProductFromDbFirst);
        }

        //Product
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Product obj)
        {

            if (ModelState.IsValid)
            {
                obj.UrlSlug = SeoUrlHepler.FrientlyUrl(obj.Name);
                _unitOfWork.ProductRepository.Update(obj);
                _unitOfWork.SaveChange();
                TempData["success"] = "Product updated successfully";
                return RedirectToAction("Index");
            }
            TempData["error"] = "Product created error";
            return View(obj);
        }



        #region API CALLS
        [HttpGet]
        public IActionResult GetAll(string status)
        {
            var productList = _unitOfWork.ProductRepository.GetAll();
            return Json(new { data = productList });
        }


        //Product
        [HttpDelete]
        [Authorize(Roles = SD.Role_Admin)]
        public IActionResult Delete(int? id)
        {
            var obj = _unitOfWork.ProductRepository.GetById(id.Value);
            if (obj == null)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }
            _unitOfWork.ProductRepository.Delete(obj);
            _unitOfWork.SaveChange();
            return Json(new { success = true, message = "Delete Successful" });

        }
        #endregion

    }
}