﻿using LapStore.Common;
using LapStore.Core.Infrastructures;
using LapStore.Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LapStore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = SD.Role_Admin)]
    public class UsersController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public UsersController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {
            //ViewBag.Abc=..
            //ViewData["Abc"]==
            return View();
        }

        //GET
        public IActionResult Create()
        {
            return View();
        }

        //User
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ApplicationUser request)
        {

            if (ModelState.IsValid)
            {

                this._unitOfWork.UserRepository.Add(request);
                _unitOfWork.SaveChange();
                TempData["success"] = "User created successfully";
                return RedirectToAction("Index");
            }
            TempData["error"] = "User created error";
            return View(request);
        }

        //GET
        public IActionResult Edit(string? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            var UserFromDbFirst = _unitOfWork.UserRepository.FindStr(id);
            if (UserFromDbFirst == null)
            {
                return NotFound();
            }

            return View(UserFromDbFirst);
        }

        //User
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(ApplicationUser obj)
        {
                _unitOfWork.UserRepository.Update(obj);
                _unitOfWork.SaveChange();
                TempData["success"] = "User updated successfully";
                return RedirectToAction("Index");
        }



        #region API CALLS
        [HttpGet]
        public IActionResult GetAll(string status)
        {
            var productList = _unitOfWork.UserRepository.GetAll();
            return Json(new { data = productList });
        }


        //User
        [HttpDelete]
        [Authorize(Roles = SD.Role_Admin)]
        public IActionResult Delete(string? id)
        {
            var obj = _unitOfWork.UserRepository.FindStr(id);
            if (obj == null)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }
            _unitOfWork.UserRepository.Delete(obj);
            _unitOfWork.SaveChange();
            return Json(new { success = true, message = "Delete Successful" });

        }
        #endregion

    }
}