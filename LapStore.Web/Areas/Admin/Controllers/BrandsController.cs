﻿using LapStore.Common;
using LapStore.Core.Infrastructures;
using LapStore.Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LapStore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = SD.Role_Admin)]
    public class BrandsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public BrandsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {

            return View();
        }

        //GET
        public IActionResult Create()
        {
            return View();
        }

        //Categorie
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Brand request)
        {
            
            if (ModelState.IsValid)
            {
                request.UrlSlug = SeoUrlHepler.FrientlyUrl(request.Name);
                this._unitOfWork.BrandRepository.Add(request);
                _unitOfWork.SaveChange();
                TempData["success"] = "Categorie created successfully";
                return RedirectToAction("Index");
            }
            TempData["error"] = "Categorie created error";
            return View(request);
        }

        //GET
        public IActionResult Edit(int? id)
        {

            if (id.Value == null || id.Value == 0)
            {
                return NotFound();
            }
            var BrandFromDbFirst = _unitOfWork.BrandRepository.GetById(id.Value);
            if (BrandFromDbFirst == null)
            {
                return NotFound();
            }
          
            return View(BrandFromDbFirst);
        }

        //Categorie
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Brand obj)
        {

            if (ModelState.IsValid)
            {
                obj.UrlSlug = SeoUrlHepler.FrientlyUrl(obj.Name);
                _unitOfWork.BrandRepository.Update(obj);
                _unitOfWork.SaveChange();
                TempData["success"] = "Categorie updated successfully";
                return RedirectToAction("Index");
            }
            TempData["error"] = "Categorie created error";
            return View(obj);
        }



        #region API CALLS
        [HttpGet]
        public IActionResult GetAll(string status)
        {
            var productList = _unitOfWork.BrandRepository.GetAll();
            return Json(new { data = productList });
        }

        //Categorie
        [HttpDelete]
        [Authorize(Roles = SD.Role_Admin)]
        public IActionResult Delete(int? id)
        {
            var obj = _unitOfWork.BrandRepository.GetById(id.Value);
            if (obj == null)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }
            _unitOfWork.BrandRepository.Delete(obj);
            _unitOfWork.SaveChange();
            return Json(new { success = true, message = "Delete Successful" });

        }
        #endregion

    }
}
