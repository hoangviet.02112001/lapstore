﻿using LapStore.Common;
using LapStore.Core.Infrastructures;
using LapStore.Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LapStore.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = SD.Role_Admin)]
    public class OrdersController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public OrdersController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {
            //ViewBag.Abc=..
            //ViewData["Abc"]==
            return View();
        }

        //GET
        public IActionResult Create()
        {
            return View();
        }

        //Order
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Order request)
        {

            if (ModelState.IsValid)
            {
                this._unitOfWork.OrderRepository.Add(request);
                _unitOfWork.SaveChange();
                TempData["success"] = "Order created successfully";
                return RedirectToAction("Index");
            }
            TempData["error"] = "Order created error";
            return View(request);
        }

        //GET
        public IActionResult Edit(int? id)
        {

            if (id.Value == null || id.Value == 0)
            {
                return NotFound();
            }
            var OrderFromDbFirst = _unitOfWork.OrderRepository.GetById(id.Value);
            if (OrderFromDbFirst == null)
            {
                return NotFound();
            }

            return View(OrderFromDbFirst);
        }

        //Order
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Order obj)
        {

            if (ModelState.IsValid)
            {
                _unitOfWork.OrderRepository.Update(obj);
                _unitOfWork.SaveChange();
                TempData["success"] = "Order updated successfully";
                return RedirectToAction("Index");
            }
            TempData["error"] = "Order created error";
            return View(obj);
        }



        #region API CALLS
        [HttpGet]
        public IActionResult GetAll(string status)
        {
            var productList = _unitOfWork.OrderRepository.GetAll(includeProperties: "User");
            return Json(new { data = productList });
        }


        //Order
        [HttpDelete]
        [Authorize(Roles = SD.Role_Admin)]
        public IActionResult Delete(int? id)
        {
            var obj = _unitOfWork.OrderRepository.GetById(id.Value);
            if (obj == null)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }
            _unitOfWork.OrderRepository.Delete(obj);
            _unitOfWork.SaveChange();
            return Json(new { success = true, message = "Delete Successful" });

        }
        #endregion

    }
}